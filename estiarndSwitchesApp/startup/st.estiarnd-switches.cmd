require asyn, 4.33
require stream, 2.7.14p
require estiarndSwitches, 0.0.1


# -----------------------------------------------------------------------------
# IOC common settings
# -----------------------------------------------------------------------------
epicsEnvSet("ASYN_PORT",      "MC_PLC1")
epicsEnvSet("IP_ADDR",        "129.129.130.174")
epicsEnvSet("PORT_ADDR",      "5000")
epicsEnvSet("PREFIX",         "PSI-ESTIARND:MC-MCU-01")

# Connection
drvAsynIPPortConfigure("$(ASYN_PORT)","$(IP_ADDR):$(PORT_ADDR)",0,0,0)
# Loading database
dbLoadRecords(estiarndSwitches.db, "P=$(PREFIX), PORT=$(ASYN_PORT), ADDR=01")

# Start the IOC
iocInit
